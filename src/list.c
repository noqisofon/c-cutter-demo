#include <stdlib.h>

#include "spagetti/list.h"

#define TRUE  1
#define FALSE 0

struct spa_list {
};

void *list_alloc() {
    void  *memp = malloc( sizeof( struct spa_list ) );

    return memp;
}

void list_free(List *self) {
    free( self );
}

List *list_init(void *memp) {
    return (List *)memp;
}

int list_is_empty(List *self) {
    return TRUE;
}
