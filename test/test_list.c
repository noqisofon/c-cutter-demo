#include <cutter.h>

#include "spagetti/list.h"


void test_new_list(void) {
    List *lst;
    lst = list_init( list_alloc() );

    // if ( list_is_empty( lst ) )
    //     PASS;
    // else
    //     FAIL;
    cut_assert( list_is_empty( lst ) );

    list_free( lst );
}
