#!/usr/bin/sh

export BASE_DIR="`dirname $0`"

if test -z "$CUTTER"; then
    CUTTER="`make -s -C $BASE_DIR echo-cutter`"
fi

$CUTTER -s $BASE_DIR "$@" $BASE_DIR
