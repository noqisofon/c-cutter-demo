#pragma once

/* #include <stdbool.h> */

typedef struct spa_list    List;


void *list_alloc    (void);

void  list_free     (List *self);

List *list_init     (void *memp);

/* _Bool list_is_empty(List *self); */
int   list_is_empty (List *self);
