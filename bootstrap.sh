#!/bin/sh

run() {
    $@

    if test $? -ne 0; then
        echo "failed $@"
        exit 1
    fi
}

run aclocal ${ALCOCAL_ARGS}
run libtoolize --copy --force
run autoheader
run automake --add-missing --foreign --copy
run autoconf
